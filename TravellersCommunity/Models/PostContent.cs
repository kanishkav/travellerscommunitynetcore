﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using TravellersCommunity.Data;

namespace TravellersCommunity.Models
{
    public class PostContent
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public ApplicationUser ApplicationUser { get; set; }
    }
}