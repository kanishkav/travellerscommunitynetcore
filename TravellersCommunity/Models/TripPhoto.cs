﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace TravellersCommunity.Models
{
    public class TripPhoto
    {
        public int Id { get; set; }
        public int TripId { get; set; }
        [ForeignKey("TripId")]
        public Trip Trip { get; set; }
        public string Image { get; set; }
        public DateTime UploadDate { get; set; }
    }
}