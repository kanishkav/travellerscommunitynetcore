﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using TravellersCommunity.LocalResources;

namespace TravellersCommunity.Models
{
    public class Location
    {
        [Required]
        public int Id { get; set; }
        [Required]
        [Display(Name ="Location",ResourceType =typeof(Resource))]
        public String Name { get; set; }
        [Display(Name ="AddressLine1",ResourceType =typeof(Resource))]

        public string AddressLine1 { get; set; }
        [Display(Name ="AddressLine2",ResourceType =typeof(Resource))]
        [Required]
        public string AddressLine2 { get; set; }
        [Display(Name ="AddressLine2",ResourceType =typeof(Resource))]
        [Required]
        public string AddressLine3 { get; set; }

        public Collection<Trip> Trips { get; set; }
        public Collection<Review> Reviews { get; set; }
    }
}