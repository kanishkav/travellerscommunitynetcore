﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using TravellersCommunity.Data;

namespace TravellersCommunity.Models
{
    public class Following
    {
        public int Id { get; set; }
        public string FollowerId { get; set; }
        
        public string FollowedId { get; set; }
        [ForeignKey("FollowerId")]
        public virtual ApplicationUser Follower { get; set; }
        [ForeignKey("FollowedId")]

        public virtual ApplicationUser Followed { get; set; }
        
    }
}