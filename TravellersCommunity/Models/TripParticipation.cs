﻿
using System.ComponentModel.DataAnnotations.Schema;
using TravellersCommunity.Data;

namespace TravellersCommunity.Models
{
    public class TripParticipation
    {
        public long Id { get; set; }
        public int TripId { get; set; }
        public string ParticipantId { get; set; }
        [ForeignKey("TripId")]
        public Trip Trip { get; set; }
        [ForeignKey("ParticipantId")]
        public ApplicationUser ApplicationUser { get; set; }


    }
}