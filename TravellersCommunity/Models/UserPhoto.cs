﻿
using System.ComponentModel.DataAnnotations;
using TravellersCommunity.Data;

namespace TravellersCommunity.Models
{
    public class UserPhoto
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Image { get; set; }
        public ApplicationUser User { get; set; }
        [Required]
        public string ApplicationUserId { get; set; }
    }
}