﻿
using System.ComponentModel.DataAnnotations;

namespace TravellersCommunity.Models
{
    public class Gender
    {
        [Key]
        public string Name { get; set; }
    }
}