﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TravellersCommunity.Models;

namespace TravellersCommunity.Dtos
{
    public class TripPhotoUpload:UserPhotoUpload
    {
        public int TripId { get; set; }
    }
}