﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TravellersCommunity.Models
{
    public class UserPhotoUpload
    {
        [Required]
        public IFormFile Image { get; set; }
    }
}