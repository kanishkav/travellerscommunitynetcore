﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TravellersCommunity.Data;

namespace TravellersCommunity.Models
{
    public class Comment
    {
        public long Id { get; set; }
        [Required]
        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public ApplicationUser ApplicationUser { get; set; }
        
        public int ReviewId { get; set; }
        
        [ForeignKey("ReviewId")]
        public virtual Review Review { get; set; }

        public DateTime Date { get; set; }
        public string Content { get; set; }

    }
}