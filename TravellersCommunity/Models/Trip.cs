﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.AccessControl;
using TravellersCommunity.Data;
using TravellersCommunity.LocalResources;

namespace TravellersCommunity.Models
{
    public class Trip
    {
        public int Id { get; set; }
        [Required]
        public string HostId { get; set; }
        [ForeignKey("HostId")]
        public ApplicationUser Host { get; set; }
        public int LocationId { get; set; }
        [ForeignKey("LocationId")]
        public Location location { get; set; }
        [Display(Name = "Start_Date", ResourceType = typeof(TravellersCommunity.LocalResources.Resource))]
        [Required(ErrorMessageResourceType = (typeof(Resource)), ErrorMessageResourceName = "PleaseprovideaValidDate")]
        public DateTime StartDate { get; set; }
        [Display(Name = "End_Date", ResourceType = typeof(Resource))]
        //[Required(ErrorMessageResourceType =typeof(Resource),ErrorMessageResourceName  ="PleaseprovideaValidDate")]
        public DateTime EndDate { get; set; }
        [Display(Name = "Description", ResourceType = typeof(Resource))]
        public string Description { get; set; }
        public Collection<TripPhoto> TripPhotos { get; set; }
    }
}