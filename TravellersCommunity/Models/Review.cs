﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using TravellersCommunity.Data;
using TravellersCommunity.LocalResources;

namespace TravellersCommunity.Models
{
    public class Review
    {
        public int Id { get; set; }
        [Required]
        public string ApplicationUserId { get; set; }
        public ApplicationUser Reviewer { get; set; }
        [Required]
        public int LocationId { get; set; }
        
        public Location Location { get; set; }
        [DisplayFormat(DataFormatString = "{0:d/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name ="Date",ResourceType =typeof(Resource))]
        public DateTime? ReviewDate { get; set; }
        public string ReviewContent { get; set; }
        public byte Rating { get; set; }
        public Collection<Comment> Comments { get; set; }
    }
}