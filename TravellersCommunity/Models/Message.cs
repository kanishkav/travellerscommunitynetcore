﻿

using System.ComponentModel.DataAnnotations.Schema;
using TravellersCommunity.Data;

namespace TravellersCommunity.Models
{
    public class Message
    {
        public int Id { get; set; }
        public string SenderId { get; set; }
        public string RecieverId { get; set; }
        [ForeignKey("SenderId")]
        public ApplicationUser Sender { get; set; }
        [ForeignKey("RecieverId")]
        public ApplicationUser Reciever { get; set; }
        public string Content { get; set; }
    }
}