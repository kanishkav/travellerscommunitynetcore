﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using TravellersCommunity.Data;
using TravellersCommunity.Dtos;
using TravellersCommunity.Models;

namespace TravellersCommunity.App_Start
{
    public class MappingProfile:Profile
    {
        public MappingProfile()
        {
            CreateMap<UserPhoto, UserPhotoDto>();
            CreateMap<UserPhotoDto, UserPhoto>();
            CreateMap<Location, LocationDto>();
            CreateMap<LocationDto, Location>();
            CreateMap<Following, FollowingDto>();
            CreateMap<FollowingDto, Following>();
            CreateMap<Message, MessageDto>();
            CreateMap<MessageDto, Message>();
            CreateMap<Review, CreateReviewDto>()
                .ForMember(cr=>cr.ReviewerId , opt=>opt.MapFrom(r=>r.ApplicationUserId));
            CreateMap<CreateReviewDto, Review>()
                .ForMember(cr=>cr.ApplicationUserId,opt=>opt.MapFrom(cr=>cr.ReviewerId));
            CreateMap<Comment, CommentDto>();
            CreateMap<CommentDto, Comment>();
            CreateMap<Trip, TripDto>();
            CreateMap<TripDto, Trip>();
            CreateMap<TripPhoto, TripPhotoDto>();
            CreateMap<TripPhotoDto, TripPhoto>();
            CreateMap<TripParticipation, TripParticipationDto>();
            CreateMap<TripParticipationDto, TripParticipation>();
            CreateMap<ApplicationUser, ViewUsersDto>();
            CreateMap<ViewUsersDto, ApplicationUser>();
            CreateMap<UserWithRoleDto, UserWithFollowingsDto>();


        }
    }
}