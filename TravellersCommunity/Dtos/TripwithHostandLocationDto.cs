﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using TravellersCommunity.LocalResources;
using TravellersCommunity.Models;

namespace TravellersCommunity.Dtos
{
    public class TripwithHostandLocationDto
    {
        public int Id { get; set; } 
        public string HostId { get; set; }
        [Display(Name ="Host",ResourceType =typeof(Resource))]
        public string Host { get; set; }
       
        public int LocationId { get; set; }
        [Display(Name ="Location",ResourceType =typeof(Resource))]
        public string LocationName { get; set; }
        [Display(Name ="Start_Date",ResourceType =typeof(Resource))]
        
        public DateTime StartDate { get; set; }
        [Display(Name ="End_Date",ResourceType =typeof(Resource))]
        public DateTime EndDate { get; set; }

        [Display(Name ="Description",ResourceType =typeof(Resource))]
        public string Description { get; set; }
    }
}