﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TravellersCommunity.Dtos
{
    public class UserMessageViewModel
    {
        public List<SpecifiedMessageDto> Messages { get; set; }
        public string MessengerId { get; set; }
        public string Messenger { get; set; }
        public string MessengerPhoto { get; set; }
    }
}