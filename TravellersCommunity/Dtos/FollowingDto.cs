﻿
using System.ComponentModel.DataAnnotations;

namespace TravellersCommunity.Dtos
{
    public class FollowingDto
    {
        public int Id { get; set; }
        [Required]
        public string FollowerId { get; set; }
        [Required]
        public string FollowedId { get; set; }
    }
}