﻿using System;

namespace TravellersCommunity.Dtos
{
    public class ForReviewCommentDto
    {
        public long Id { get; set; }
        public string UserId { get; set; }
        public string CommentedUser { get; set; }
        public string Photo { get; set; }
        public int ReviewId { get; set; }
        public DateTime Date { get; set; }
        public string Content { get; set; }
    }
}