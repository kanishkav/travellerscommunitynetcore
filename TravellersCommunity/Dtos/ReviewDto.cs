﻿using System;
using System.ComponentModel.DataAnnotations;
using TravellersCommunity.LocalResources;

namespace TravellersCommunity.Dtos
{
    public class ReviewDto
    {
        public int Id { get; set; }
        public string ReviewerId { get; set; }
        [Display(Name ="Location",ResourceType =typeof(Resource))]
        public string Reviewer { get; set; }
        public string ReviewerPhoto { get; set; }
        public int LocationId { get; set; }
        [Display(Name ="Location",ResourceType =typeof(Resource))]
        public string Location { get; set; }
        
        [DisplayFormat(DataFormatString = "{0:d/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name ="Date",ResourceType =typeof(Resource))]
        public DateTime? ReviewDate { get; set; }
        [Display(Name ="Content",ResourceType =typeof(Resource))]
        public string ReviewContent { get; set; }
        [Display(Name ="Rating",ResourceType =typeof(Resource))]
        public byte Rating { get; set; }
    }
}