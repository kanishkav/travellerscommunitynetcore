﻿

namespace TravellersCommunity.Dtos
{
    public class UserPhotoDto
    {
        public int Id { get; set; }
        public string Image { get; set; }
        public string ApplicationUserId { get; set; }
    }
}