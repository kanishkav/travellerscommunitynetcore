﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TravellersCommunity.Dtos
{
    public class TripDto
    {
        public int Id { get; set; }
        [Required]
        public string HostId { get; set; }
        [Required]
        public int LocationId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Description { get; set; }
    }
}