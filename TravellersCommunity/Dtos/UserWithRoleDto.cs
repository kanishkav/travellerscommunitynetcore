﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TravellersCommunity.LocalResources;

namespace TravellersCommunity.Dtos
{
    public class UserWithRoleDto
    {
        public string Id { get; set; }
        public string Email { get; set; }
        [Display(Name ="UserName",ResourceType =typeof(Resource))]
        public string UserName { get; set; }
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]

        [Display(Name ="BirthDay",ResourceType =typeof(Resource))]
        public DateTime? Birthday { get; set; }
        [Display(Name ="Location",ResourceType =typeof(Resource))]
        public string Location { get; set; }
        [Display(Name ="Gender",ResourceType =typeof(Resource))]
        public string Gender { get; set; }
        public string Photo { get; set; }
        public string RoleId { get; set; }
        [Display(Name ="Role",ResourceType =typeof(Resource))]
        public string Role { get; set; }
    }
}