﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TravellersCommunity.Dtos
{
    public class CommentDto
    {
        public long Id { get; set; }
        [Required]
        public string UserId { get; set; }
        [Required]
        public int ReviewId { get; set; }

        public DateTime? Date { get; set; }
        public string Content { get; set; }
    }
}