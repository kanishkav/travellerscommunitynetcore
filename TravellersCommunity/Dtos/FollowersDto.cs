﻿using System;

namespace TravellersCommunity.Dtos
{
    public class FollowersDto
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string Photo { get; set; }

    }
}