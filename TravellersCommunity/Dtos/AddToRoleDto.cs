﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TravellersCommunity.Dtos
{
    public class AddToRoleDto
    {
        public string UserId { get; set; }
        public string Role { get; set; }
    }
}