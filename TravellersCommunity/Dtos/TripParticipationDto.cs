﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace TravellersCommunity.Dtos
{
    public class TripParticipationDto
    {
        public long Id { get; set; }
        [Required]
        public int TripId { get; set; }
        [Required]
        public string ParticipantId { get; set; }

    }
}