﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;
using TravellersCommunity.LocalResources;

namespace TravellersCommunity.Dtos
{
    public class MessagedUsersDto
    {
        public string Id { get; set; }
        public string Email { get; set; }
        [Display(Name ="User",ResourceType =typeof(Resource))]
        public string UserName { get; set; }
        [Display(Name ="Photo",ResourceType =typeof(Resource))]
        public string Photo { get; set; }
        public bool? Messaged { get; set; }
    }
}