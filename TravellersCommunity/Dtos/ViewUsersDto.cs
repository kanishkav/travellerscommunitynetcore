﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TravellersCommunity.Dtos
{
    public class ViewUsersDto
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public DateTime Birthday { get; set; }
        public string Location { get; set; }
        public string Gender { get; set; }
        public string Photo { get; set; }
    }
}