﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TravellersCommunity.Models;

namespace TravellersCommunity.Dtos
{
    public class TripDetailViewModel
    {
        public TripwithHostandLocationDto Trip { get; set; }
        public IEnumerable<TripParticipantsDetailDto> Participants { get; set; }
    }
}