﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TravellersCommunity.Dtos
{
    public class TripPhotoDto
    {
        public int Id { get; set; }
        [Required]
        public int TripId { get; set; }
        [Required]
        public string Image { get; set; }
        public DateTime UploadDate { get; set; }
    }
}