﻿using System;

namespace TravellersCommunity.Dtos
{
    public class GetTripDto
    {
        public int Id { get; set; }
        public string HostId { get; set; }
        public string HostUser { get; set; }
        public string HostPhoto { get; set; }
        public int LocationId { get; set; }
        public string Location { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Description { get; set; }
    }
}