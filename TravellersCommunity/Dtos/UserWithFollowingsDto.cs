﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TravellersCommunity.LocalResources;

namespace TravellersCommunity.Dtos
{
    public class UserWithFollowingsDto
    {
        public string Id { get; set; }
        public string Email { get; set; }
        [Display(Name ="UserName",ResourceType =typeof(Resource))]
        public string UserName { get; set; }
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Birthday { get; set; }
        public string Location { get; set; }
        public string Gender { get; set; }
        [Display(Name ="Photo",ResourceType =typeof(Resource))]
        public string Photo { get; set; }
        public string RoleId { get; set; }
        [Display(Name ="Role",ResourceType =typeof(Resource))]
        public string Role { get; set; }
        [Display(Name ="Follow",ResourceType =typeof(Resource))]
        public bool? Following { get; set; }
    }
}