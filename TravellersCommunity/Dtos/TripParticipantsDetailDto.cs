﻿

using System.ComponentModel.DataAnnotations;

namespace TravellersCommunity.Dtos
{
    public class TripParticipantsDetailDto
    {
        public long Id { get; set; }
        public int TripId { get; set; }
        public string ParticipantId { get; set; }
        public string ParticipantName { get; set; }
        public string ParticipantPhoto { get; set; }
    }
}