﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TravellersCommunity.Dtos
{
    public class CreateReviewDto
    {
        public int Id { get; set; }
        [Required]
        public string ReviewerId { get; set; }
        [Required]
        public int LocationId { get; set; }
        public DateTime? ReviewDate { get; set; }
        public string ReviewContent { get; set; }
        [Range(0,5)]
        public byte Rating { get; set; }
    }
}