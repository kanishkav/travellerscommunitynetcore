﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TravellersCommunity.Models;

namespace TravellersCommunity.Dtos
{
	public class PersonalDetailsWithFollowersViewModel
	{
        public UserWithRoleDto User { get; set; }
        public List<FollowersDto> followers { get; set; }
    }
}