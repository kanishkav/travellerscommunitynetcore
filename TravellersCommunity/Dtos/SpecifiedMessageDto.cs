﻿

namespace TravellersCommunity.Dtos
{
    public class SpecifiedMessageDto
    {
        public int Id { get; set; }
        public string Owner { get; set; }
        public string MessengerId { get; set; }
        public string MessengerPhoto { get; set; }
        public string MessageUser { get; set; }
        public string Content  { get; set; }

        public string Type { get; set; }
    }
}