﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TravellersCommunity.Dtos
{
    public class LocationDto
    {
        
        public int Id { get; set; }
        [Required]
        [Key]
        public String Name { get; set; }

        public string AddressLine1 { get; set; }
        [Required]
        public string AddressLine2 { get; set; }
        [Required]
        public string AddressLine3 { get; set; }
    }
}