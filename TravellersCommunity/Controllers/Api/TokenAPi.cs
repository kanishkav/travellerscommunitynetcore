﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using TravellersCommunity.Data;
using TravellersCommunity.Models;

namespace TravellersCommunity.Controllers.Api
{
    
    [ApiController]

    public class TokenAPi : ControllerBase
    {
        private IConfiguration _config;
        private ApplicationDbContext _db;

        private UserManager<ApplicationUser> _userManager;
        private SignInManager<ApplicationUser> _signInManager;


        public TokenAPi(
            IConfiguration config,
            ApplicationDbContext db,
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager
            )
        {
            _config = config;
            _db = db;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("/token")]
        public IActionResult Login([FromBody] RequestToken login)
        {
            IActionResult response = Unauthorized();
            var user = AuthenticateUser(login);

            if (user != null)
            {
                var tokenString = GenerateJSONWebToken(user);
                response = Ok(new { token = tokenString });
            }

            return response;
        }
        [Authorize]
        [HttpGet]
        [Route("/Test")]
        public IActionResult Test()
        {
            var x = new RequestToken
            {
                UserName = "Tester",
                Password = "Tester"
            };

            return Ok(x);
        }

        private string GenerateJSONWebToken(ApplicationUser userInfo)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var role = _userManager.GetRolesAsync(userInfo);

            var claims = new[] {
        new Claim(ClaimTypes.NameIdentifier, userInfo.Id),
        new Claim(ClaimTypes.Name, userInfo.UserName),
        new Claim(ClaimTypes.Email, userInfo.Email),
        new Claim(ClaimTypes.Role, role.Result[0]),
        new Claim("UserId", userInfo.Id)
    };
            var token = new JwtSecurityToken(_config["Jwt:Issuer"],
              _config["Jwt:Issuer"],
              claims,
              expires: DateTime.Now.AddMinutes(120),
              signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private ApplicationUser AuthenticateUser(RequestToken login)
        {
            var user = _userManager.Users.SingleOrDefault(u => u.UserName == login.UserName);
            var result = _signInManager.PasswordSignInAsync(user, login.Password, false, false);

            if (!result.Result.Succeeded)
            {
                user = null;
            }
            return user;



        }
    }
}
