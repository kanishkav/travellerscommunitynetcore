﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;

namespace TravellersCommunity.Controllers.Api
{
    public class BaseApiController : ControllerBase
    {
        protected IActionResult onError(Exception e, NLog.Logger _logger)
        {
            _logger.Error(e, e.StackTrace);
            return new StatusCodeResult(StatusCodes.Status500InternalServerError);
        }

        
    }
}
