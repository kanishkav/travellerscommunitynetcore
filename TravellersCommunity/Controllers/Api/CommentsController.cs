﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Xml.XPath;
using TravellersCommunity.Data;
using TravellersCommunity.Dtos;
using TravellersCommunity.Models;
using TravellersCommunity.Repositories;

namespace TravellersCommunity.Controllers.Api
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class CommentsController : BaseApiController
    {

        
        private IMapper _mapper;
        private static readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        private ICommentRepo _repo;
        private ICommentRepository _repository;
        private ApplicationDbContext db;


        public CommentsController(

            IMapper mapper,
            ICommentRepo repo,
            ICommentRepository repository)
        {
            _repository = repository;
            
            _mapper = mapper;
            _repo = repo;

        }
        // GET: api/Comments
        [HttpGet]
        public IActionResult GetComments()
        {
            try
            {
                return Ok(_repository.GetAll().Select(_mapper.Map<Comment, CommentDto>).ToList());
                //return Ok(_repo.GetAllComments().Select(_mapper.Map<Comment, CommentDto>).ToList());
                //return Ok(db.Comments.Select(_mapper.Map<Comment, CommentDto>).ToList());

            }
            catch (Exception e)
            {

                return onError(e, _logger);
            }
        }

        //GET: api/Comments/ForReview/4
        [HttpGet]
        [Route("ForReview/{id}")]
        public object CommentsForReview(int id)
        {
            if (id == null)
            {
                return NotFound();
            }
            try
            {

                //var review = db.Reviews.FromSqlRaw($"dbo.Review_Get @Id",
                //    new SqlParameter("Id", id)).ToList();

                //if (review.Count == 0)
                //    return NotFound();


                //var comments = db.ReviewCommentDto.FromSqlRaw($"dbo.Comment_GetDetailedByReviewId @ReviewId",
                //    new SqlParameter("ReviewId", id)).ToList();
                var comments = _repository.GetCommentsOfReview(id);

                return comments;
            }
            catch (Exception e)
            {
                return onError(e, _logger);
            }
        }

        // GET: api/Comments/5
        [Route("{id}")]
        [HttpGet]
        public async Task<IActionResult> GetComments(int id)
        {
            if (id == null)
                return BadRequest();

            try
            {
                //var comment = _repo.GetComment(id);
                var comment = _repository.Get(id);
                if (comment == null)
                {
                    return NotFound();
                }

                return Ok(_mapper.Map<Comment, CommentDto>(comment));

            }
            catch (Exception e)
            {

                return onError(e,_logger);
            }
        }



        // POST: api/Comments
        [HttpPost]
        
        public async Task<IActionResult> AddComment(CommentDto commentDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {

                //var user = db.Users.FromSqlRaw($"ApplicationUser_GetbyId @Id",
                //        new SqlParameter("Id", commentDto.UserId)).ToList();
                //if (user.Count == 0)
                //    return BadRequest("User Ids not found");

                //var review = db.Reviews.FromSqlRaw($"dbo.Review_Get @Id",
                //    new SqlParameter("Id", commentDto.ReviewId)).ToList();
                //if (review.Count == 0)
                //    return BadRequest("Review not found");

                commentDto.Date = DateTime.Today;
                var comment = _mapper.Map<CommentDto, Comment>(commentDto);

                var result = _repository.Add(comment);
                //await db.SaveChangesAsync();
                _logger.Info($"Created Comment {result.First()} by {commentDto.UserId}");
                commentDto.Id = result.First().Id;

                return CreatedAtAction(nameof(GetComments), new { id = commentDto.Id }, commentDto);
            }
            catch (Exception e)
            {
                return onError(e,_logger);
            }
        }


        // PUT: api/Comments/5
        [HttpPut]
        [Route("{id}")]
        public async Task<IActionResult> PutComment(int id, CommentDto commentDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != commentDto.Id)
            {
                return BadRequest();
            }
            try
            {
                var cUser = ((ClaimsIdentity)User.Identity).Claims.FirstOrDefault(c=>c.Type=="UserId").Value;

                if (cUser == null)
                    return BadRequest();


                if (!(commentDto.UserId == cUser))
                    return BadRequest("only commenter can change the comment");

                var commentInDb = _repository.Get(id);
                var comment = _mapper.Map<CommentDto, Comment>(commentDto);
                comment.Id = commentInDb.Id;
                comment.UserId = commentInDb.UserId;
                comment.ReviewId = commentInDb.ReviewId;
                comment.Date = DateTime.Today;
                _repository.UpdateComment(id, comment);
                //db.Database.ExecuteSqlRaw($"dbo.Comment_Update @Id,@UserId,@ReviewId,@Date,@Content",
                //    new SqlParameter("Id", commentInDb.First().Id),
                //    new SqlParameter("UserId", commentInDb.First().UserId),
                //    new SqlParameter("ReviewId", commentInDb.First().ReviewId),
                //    new SqlParameter("Date", commentDto.Date),
                //    new SqlParameter("Content", commentDto.Content)
                //    );
                _logger.Info($"Edited Comment {id} by {commentDto.UserId}");

                //db.SaveChanges();

                return NoContent();
            }
            catch (Exception e)
            {

                return onError(e,_logger);
            }




        }
        // DELETE: api/Comments/5
        [HttpDelete]
        [Route("{id}")]
        public async Task<IActionResult> DeleteComment(int id)
        {
            if (id == null)
                return BadRequest();

            try
            {

                //var commentInDb = db.Comments.FromSqlRaw($"dbo.Comment_Get @Id",
                //        new SqlParameter("Id", id)).ToList();
                var commentInDb = _repository.Get(id);

                if (commentInDb==null)
                {
                    return NotFound();
                }
                //var loggedUser = ((ClaimsIdentity)User.Identity).Claims.FirstOrDefault(c=>c.Type=="UserId").Value;
               // if (!(commentInDb.First().UserId == loggedUser
                    //|| User.IsInRole("Admin User")))
                   // return BadRequest("only commenter can Delete the comment");

                var commentDto = _mapper.Map<Comment, CommentDto>(commentInDb);

                _repository.Remove(commentInDb);
                //db.Database.ExecuteSqlRaw($"dbo.Comment_Delete @Id",
                //    new SqlParameter("Id", id));
                //await db.SaveChangesAsync();
               // _logger.Info($"Deleted the comment {id} by {loggedUser}");
                return Ok(commentDto);
            }
            catch (Exception e)
            {

                return onError(e,_logger);
            }
        }

        
       
        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
