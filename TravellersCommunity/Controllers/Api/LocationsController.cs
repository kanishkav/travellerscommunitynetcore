﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using TravellersCommunity.Data;
using TravellersCommunity.Dtos;
using TravellersCommunity.Models;

namespace TravellersCommunity.Controllers.Api
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class LocationsController : BaseApiController
    {
        private readonly ApplicationDbContext db;
        private static readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        private IMapper _mapper;
        

        public LocationsController(
            ApplicationDbContext _db,
            IMapper mapper)
        {
            db = _db;
            _mapper = mapper;
           
        }

        
        // GET: api/Locations
        [HttpGet]
        public object GetLocations()
        {
            try
            {
                return db.Locations.FromSqlRaw($"dbo.Location_GetAll").Select(_mapper.Map<Location, LocationDto>);
                _logger.Info("testing");
            }
            catch (Exception e)
            {
                return onError(e,_logger);
            }
        }

        // GET: api/Locations/5
        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetLocation(int id)
        {
            try
            {

                var location = db.Locations.FromSqlRaw($"dbo.Location_Get @Id", new SqlParameter("Id", id)).ToList();
                if (location.Count == 0)
                {
                    return NotFound();
                }

                return Ok(_mapper.Map<Location, LocationDto>(location.First()));
            }
            catch (Exception e)
            {
                return onError(e,_logger);
            }
        }

        // PUT: api/Locations/5
        [Authorize(Roles = "Admin User,Premium User")]
        [HttpPut]
        [Route("{id}")]
        public async Task<IActionResult> PutLocation(int id, LocationDto locationDto)
        {
            try
            {

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                if (id != locationDto.Id)
                {
                    return BadRequest();
                }
                var location = db.Locations.FromSqlRaw($"dbo.Location_Get @Id", new SqlParameter("Id", id)).ToList();
                if (location.Count == 0)
                {
                    return NotFound();
                }

                db.Database.ExecuteSqlRaw($"dbo.Location_Update @Id,@Name,@AddressLine1,@AddressLine2,@AddressLine3",
                   new SqlParameter("Id", locationDto.Id),
                   new SqlParameter("Name", locationDto.Name),
                   new SqlParameter("AddressLine1", locationDto.AddressLine1),
                   new SqlParameter("AddressLine2", locationDto.AddressLine2),
                   new SqlParameter("AddressLine3", locationDto.AddressLine3));


                await db.SaveChangesAsync();
                 var loggedUserId = ((ClaimsIdentity)User.Identity).Claims.SingleOrDefault(c => c.Type == "UserId").Value;


                _logger.Info($"Edited Location {id} by {loggedUserId}");
                return NoContent();
            }
            catch (Exception e)
            {
                return onError(e,_logger);
            }
        }

        // POST: api/Locations
        [Authorize(Roles = "Admin User,Premium User")]
        [HttpPost]
        public async Task<IActionResult> PostLocation(LocationDto locationDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                
                var location = _mapper.Map<LocationDto, Location>(locationDto);

                var result = db.Locations.FromSqlRaw($"dbo.Location_Insert @Name,@AddressLine1,@AddressLine2,@AddressLine3;SELECT * FROM Comments WHERE Id=scope_identity();",

                    new SqlParameter("Name", locationDto.Name),
                    new SqlParameter("AddressLine1", locationDto.AddressLine1),
                    new SqlParameter("AddressLine2", locationDto.AddressLine2),
                    new SqlParameter("AddressLine3", locationDto.AddressLine3)).ToList();
                await db.SaveChangesAsync();

                 var loggedUserId = ((ClaimsIdentity)User.Identity).Claims.SingleOrDefault(c => c.Type == "UserId").Value;
                _logger.Info($"created Location {result.First().Id} by {loggedUserId}");
                locationDto.Id = result.First().Id;

                return CreatedAtAction(nameof(GetLocation), new { id = locationDto.Id }, locationDto);
            }
            catch (Exception e)
            {

                return onError(e,_logger);
            }

        }

        // DELETE: api/Locations/5
        [Authorize(Roles = "Admin User")]
        [HttpDelete]
        [Route("{id}")]
        public async Task<IActionResult> DeleteLocation(int id)
        {
            try
            {

                var location = db.Locations.FromSqlRaw($"dbo.Location_Get @Id", new SqlParameter("Id", id)).ToList();

                if (location.Count == 0)
                {
                    return NotFound();
                }
                db.Database.ExecuteSqlRaw($"dbo.Location_Delete @Id", new SqlParameter("Id", id));

                await db.SaveChangesAsync();

                return Ok(_mapper.Map<Location, LocationDto>(location.First()));
            }
            catch (Exception e)
            {

                return onError(e,_logger);
            }
        }

        
        


    }
}