﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using TravellersCommunity.Data;
using TravellersCommunity.Models;

namespace TravellersCommunity.Controllers.Api
{
    public class AccountApiController : BaseApiController
    {
        private ApplicationDbContext db;

        private UserManager<ApplicationUser> _userManager;
        private static readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        public AccountApiController(
            UserManager<ApplicationUser> userManager,
            ApplicationDbContext _db)
        {

            _userManager = userManager;
            db = _db;
        }
        //POST api/register
        [HttpPost]
        [Route("api/register")]
        public async Task<object> RegisterUser(RegisterViewModel userModel)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            try
            {

                ApplicationUser user = new ApplicationUser
                {
                    UserName = userModel.Email,
                    Email = userModel.Email
                };

                var result = await _userManager.CreateAsync(user, userModel.Password);


                var userInDb = db.Users.FirstOrDefault(u => u.Email == userModel.Email);

                _logger.Info($"Registered the user {userInDb.Id}");

                if (!result.Succeeded)
                    return BadRequest(result.Errors.ToString());
                _userManager.AddToRoleAsync(userInDb, "User");

                return Ok();
            }
            catch (Exception e)
            {

                return onError(e,_logger);
            }
        }


        
        public void Dispose()
        {
            
            db.Dispose();
            _userManager.Dispose();

        }
    }
}
