﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using TravellersCommunity.Data;

namespace TravellersCommunity.Repositories
{
    public class Repository<EntityT> : IRepository<EntityT> where EntityT : class
    {
        protected readonly ApplicationDbContext _context;
        public Repository(ApplicationDbContext context)
        {
            _context = context;
        }
        public void Add(EntityT entity)
        {
            _context.Set<EntityT>().Add(entity);
        }

        public void AddRange(IEnumerable<EntityT> entities)
        {
            _context.Set<EntityT>().AddRange(entities);
        }

        public IEnumerable<EntityT> Find(Expression<Func<EntityT, bool>> predicate)
        {
            return _context.Set<EntityT>().Where(predicate);
        }

        public EntityT Get(int id)
        {
            return _context.Set<EntityT>().Find(id);
        }

        public IEnumerable<EntityT> GetAll()
        {
            return _context.Set<EntityT>().ToList();
        }

        public void Remove(EntityT entity)
        {
            _context.Set<EntityT>().Remove(entity);
        }

        public void RemoveRange(IEnumerable<EntityT> entities)
        {
            _context.Set<EntityT>().RemoveRange(entities);
        }
    }
}
