﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TravellersCommunity.Dtos;
using TravellersCommunity.Models;

namespace TravellersCommunity.Repositories
{
    public interface ICommentRepository:IRepository<Comment>
    {
        IEnumerable<ForReviewCommentDto> GetCommentsOfReview(int id);
        new IEnumerable<Comment> Add(Comment comment);
        void UpdateComment(int id, Comment comment);
    }
}
