﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using TravellersCommunity.Data;
using TravellersCommunity.Dtos;
using TravellersCommunity.Models;

namespace TravellersCommunity.Repositories
{
    public class CommentRepository : Repository<Comment>, ICommentRepository
    {
        public CommentRepository(ApplicationDbContext context):base(context)
        {

        }

        public new Comment Get(int id)
        {
            var comment = _context.Comments.FromSqlRaw($"dbo.Comment_Get @Id",
                    new SqlParameter("Id", id)).ToList();
            if (comment.Count == 0)
                return null;

            return comment.First();
        }

        public new void Remove(Comment comment)
        {
            _context.Database.ExecuteSqlRaw($"dbo.Comment_Delete @Id",
                    new SqlParameter("Id", comment.Id));
        }
        public IEnumerable<ForReviewCommentDto> GetCommentsOfReview(int id)
        {
            var comments = _context.ReviewCommentDto.FromSqlRaw($"dbo.Comment_GetDetailedByReviewId @ReviewId",
                    new SqlParameter("ReviewId", id)).ToList();
            return comments;
        }

        public new IEnumerable<Comment> Add(Comment comment)
        {
            var result = _context.Comments.FromSqlRaw($"dbo.Comment_Insert @UserId,@ReviewId,@Date,@Content",
                    new SqlParameter("UserId", comment.UserId),
                    new SqlParameter("ReviewId", comment.ReviewId),
                    new SqlParameter("Date", comment.Date),
                    new SqlParameter("Content", comment.Content)
                    ).ToList();
            return result;
        }

        public void UpdateComment(int id, Comment comment)
        {
            _context.Database.ExecuteSqlRaw($"dbo.Comment_Update @Id,@UserId,@ReviewId,@Date,@Content",
                    new SqlParameter("Id", comment.Id),
                    new SqlParameter("UserId", comment.UserId),
                    new SqlParameter("ReviewId", comment.ReviewId),
                    new SqlParameter("Date", comment.Date),
                    new SqlParameter("Content", comment.Content)
                    );
        }
    }
}
