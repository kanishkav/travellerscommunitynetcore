﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace TravellersCommunity.Repositories
{

    
    public interface IRepository<EntityT> where EntityT : class
    {
        EntityT Get(int id);
        IEnumerable<EntityT> GetAll();
        IEnumerable<EntityT> Find(Expression<Func<EntityT, bool>> predicate);

        void Add(EntityT entity);
        void AddRange(IEnumerable<EntityT> entities);

        void Remove(EntityT entity);
        void RemoveRange(IEnumerable<EntityT> entities);
    }
}
