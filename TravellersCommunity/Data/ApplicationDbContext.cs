﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using TravellersCommunity.Dtos;
using TravellersCommunity.Models;

namespace TravellersCommunity.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Gender> Genders { get; set; }
        public DbSet<UserPhoto> UserPhotos { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<Following> Followings { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Trip> Trips { get; set; }

        public DbSet<TripPhoto> TripPhotos { get; set; }

        public DbSet<TripParticipation> TripParticipations { get; set; }


        //query db sets

        public DbSet<ForReviewCommentDto> ReviewCommentDto { get; set; }

        
        

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {

            base.OnModelCreating(builder);
            builder.Entity<ForReviewCommentDto>().HasNoKey().ToView(null);
        }
    }

    public class ApplicationUser: IdentityUser
    {
        public DateTime? BirthDate { get; set; }

        public String Location { get; set; }
        public Gender Gender { get; set; }
        public String UserPhoto { get; set; }
        public Collection<Review> Reviews { get; set; }
        public Collection<Trip> Trips { get; set; }
        public Collection<TripParticipation> TripParticipations { get; set; }
    }

    public interface IApplicationDbContext
    {
        public DbSet<Gender> Genders { get; set; }
        public DbSet<UserPhoto> UserPhotos { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<Following> Followings { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Trip> Trips { get; set; }

        public DbSet<TripPhoto> TripPhotos { get; set; }

        public DbSet<TripParticipation> TripParticipations { get; set; }


        //query db sets

        public DbSet<ForReviewCommentDto> ReviewCommentDto { get; set; }

        public int SaveChanges();
    }

    public class CommentRepo:ICommentRepo
    {
        ApplicationDbContext _db;
        
        public CommentRepo(ApplicationDbContext db)
        {
            _db = db;
        }

        public  List<Comment> GetAllComments()
        {
            return _db.Comments.ToList();
        }

        public Comment GetComment(int id)
        {
            return _db.Comments.FromSqlRaw($"dbo.Comment_Get @Id",
                    new SqlParameter("Id", id)).ToList().SingleOrDefault(c=>c.Id!=null);
        }
    }

    public interface ICommentRepo
    {
         List<Comment> GetAllComments();
        Comment GetComment(int id);
    }
}
